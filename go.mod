module github.com/ErickG-ISC/Odbc-go2

go 1.22.1

require (
	filippo.io/edwards25519 v1.1.0 // indirect
	github.com/alexbrainman/odbc v0.0.0-20230814102256-1421b829acc9 // indirect
	github.com/go-sql-driver/mysql v1.8.0 // indirect
	github.com/gorilla/mux v1.8.1 // indirect
	github.com/jmoiron/sqlx v1.3.5 // indirect
	github.com/lib/pq v1.10.9 // indirect
	github.com/mattn/go-sqlite3 v1.14.22 // indirect
	golang.org/x/sys v0.0.0-20190916202348-b4ddaad3f8a3 // indirect
)
