package main

//Paquetes necesarios para ejecutar el programa
import (
	"context"
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

// Estructura de Category
type Category struct {
	CategoryID   int64
	CategoryName string
	Description  string
}

// Main
func main() {
	ctx := context.Background()

	db, err := createConnection()
	if err != nil {
		panic(err)
	}

	err = queryCategories(ctx, db)
	if err != nil {
		panic(err)
	}

	db.Close()
}

// Crea la conexión a la base de datos
func createConnection() (*sql.DB, error) {
	connectionString := "root:1234@tcp(localhost:3306)/northwind" //ususario:contraseña@tcp(servidor)/base de datos

	db, err := sql.Open("mysql", connectionString)
	if err != nil {
		return nil, err
	}

	db.SetMaxOpenConns(5)

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return db, nil
}

// Muestra los registros de la tabla de categories
func queryCategories(ctx context.Context, db *sql.DB) error {
	qry := `
		select
			cat.CategoryID,
			cat.CategoryName,
			cat.Description
		from categories cat`

	rows, err := db.QueryContext(ctx, qry)
	if err != nil {
		return nil
	}

	categories := []Category{}

	for rows.Next() {
		cat := Category{}
		err := rows.Scan(&cat.CategoryID, &cat.CategoryName, &cat.Description)
		if err != nil {
			return nil
		}

		categories = append(categories, cat)
	}

	fmt.Println(categories)

	return nil
}
